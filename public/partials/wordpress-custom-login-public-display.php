<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://demoify.com
 * @since      1.0.0
 *
 * @package    Wordpress_Custom_Login
 * @subpackage Wordpress_Custom_Login/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
