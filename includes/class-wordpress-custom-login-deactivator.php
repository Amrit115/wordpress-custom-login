<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://demoify.com
 * @since      1.0.0
 *
 * @package    Wordpress_Custom_Login
 * @subpackage Wordpress_Custom_Login/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wordpress_Custom_Login
 * @subpackage Wordpress_Custom_Login/includes
 * @author     Demoify <info@demoify.com>
 */
class Wordpress_Custom_Login_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
