<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://demoify.com
 * @since             1.0.0
 * @package           Wordpress_Custom_Login
 *
 * @wordpress-plugin
 * Plugin Name:       WordPress Custom Login
 * Plugin URI:        http://demoify.com/plugin/wordpress-custom-login
 * Description:       WordPress custom login plugin helps to cusotomize your WordPress login screen Amazingly. It helps you to add logo, change background color as well as add Social Link.
 * Version:           1.0.0
 * Author:            Demoify
 * Author URI:        http://demoify.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wordpress-custom-login
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wordpress-custom-login-activator.php
 */
function activate_wordpress_custom_login() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wordpress-custom-login-activator.php';
	Wordpress_Custom_Login_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wordpress-custom-login-deactivator.php
 */
function deactivate_wordpress_custom_login() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wordpress-custom-login-deactivator.php';
	Wordpress_Custom_Login_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wordpress_custom_login' );
register_deactivation_hook( __FILE__, 'deactivate_wordpress_custom_login' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wordpress-custom-login.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wordpress_custom_login() {

	$plugin = new Wordpress_Custom_Login();
	$plugin->run();

}
run_wordpress_custom_login();
